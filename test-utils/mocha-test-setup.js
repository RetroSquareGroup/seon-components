// mocha-test-setup.js
var chai = require('chai');
var spies = require('chai-spies');
var jsdom = require('jsdom').jsdom;

chai.use(spies);

// prevent tests from being ran in another mode
// WARNING: this is important otherwise it is possible to lose DB data
process.env.NODE_ENV = 'test';

var exposedProperties = ['window', 'navigator', 'document'];

if (!global.document || !global.window) {
  global.document = jsdom('<!doctype html><html><body></body></html>', {
    url: 'http://journalous',
    cookie: 'csrftoken=123; path=/'
  });
  global.window = document.defaultView;
	Object.keys(document.defaultView).forEach((property) => {
		if (typeof global[property] === 'undefined') {
			exposedProperties.push(property);
			global[property] = document.defaultView[property];
		}
	});
  global.navigator = {
		userAgent: 'node.js'
	};

  // more robust modern browser mimicking
  const docCreateElement = document.createElement.bind(global.document);
  global.document.createElement = function(type) {
    const element = docCreateElement(type);
    switch (type) {
      case 'input':
        element.multiple = true;
      break;
    }
    return element;
  };

  window.addEventListener('load', () => {});
}
