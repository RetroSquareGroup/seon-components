// mocha-dummy-compiler.js

const Module = require('module');
const hook = require('css-modules-require-hook');

function noop() { return null; }

// Hack for resolving webpack loader dependencies without webpack
var _resolver = Module._resolveFilename;
Module._resolveFilename = function(request, parent) {
  if (request.indexOf('!') === 0) {
    request = request.split('!').pop();
  }
  return _resolver(request, parent);
};

require.extensions['.md'] = noop;

hook({
    generateScopedName: '[name]__[local]___[hash:base64:5]'
});
