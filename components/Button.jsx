require('./base.css');

import React from 'react';
import classnames from 'classnames';

import styles from './button.css';

class Button extends React.Component {
  render() {
    const {
      children,
      className,
      ...props
    } = this.props;

    return (
      <button
        className={classnames(styles.button, className)}
        {...props}
      >
        {children}
      </button>
    );
  }
}

export default Button;
