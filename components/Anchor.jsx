require('./base.css');

import React, {PropTypes} from 'react';
import classnames from 'classnames';

import styles from './anchor.css';

class Anchor extends React.Component {
  render() {
    const {
      children,
      className,
      href,
      ...props
    } = this.props;

    return (
      <a
        href={href}
        className={classnames(styles.anchor, className)}
        {...props}
      >
        {children}
      </a>
    );
  }
}

Anchor.propTypes = {
  href: PropTypes.string.isRequired
};

export default Anchor;

