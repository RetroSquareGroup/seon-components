require('babel-core/register');

exports['default'] = {
  Anchor: require('components/Anchor'),
  Button: require('components/Button'),
  Checkbox: require('components/Checkbox'),
  DropDownButton: require('components/DropDownButton'),
  Input: require('components/Input'),
  LinkButton: require('components/LinkButton'),
  ProgressBar: require('components/ProgressBar'),
  RadioGroup: require('components/RadioGroup'),
  Select: require('components/Select'),
  Tabs: require('components/Tabs'),
  TextArea: require('components/TextArea')
};

module.exports = exports['default'];
