import React from 'react';
import ReactDOM from 'react-dom';

import Anchor from '../components/Anchor';
import Button from '../components/Button';
import Checkbox from '../components/Checkbox';
import DropDownButton from '../components/DropDownButton';
import Input from '../components/Input';
import LinkButton from '../components/LinkButton';
import ProgressBar from '../components/ProgressBar';
import RadioGroup from '../components/RadioGroup';
import Select from '../components/Select';
import Tabs from '../components/Tabs';
import TextArea from '../components/TextArea';

import styles from './explorer.css';

class App extends React.Component {
  render() {
    return (
      <div>

        <header>
          <h1>Seon Components</h1>
        </header>

        <section>
          <h2>Anchor</h2>
          <Anchor href="#">Link</Anchor>
        </section>

        <section>
          <h2>Button</h2>
          <Button>Button</Button>
        </section>

        <section>
          <h2>Checkbox</h2>
          <Checkbox />
        </section>

        <section>
          <h2>DropDownButton</h2>
          <DropDownButton />
        </section>

        <section>
          <h2>Input</h2>
          <Input />
        </section>

        <section>
          <h2>LinkButton</h2>
          <LinkButton />
        </section>

        <section>
          <h2>ProgressBar</h2>
          <ProgressBar />
        </section>

        <section>
          <h2>RadioGroup</h2>
          <RadioGroup />
        </section>

        <section>
          <h2>Select</h2>
          <Select />
        </section>

        <section>
          <h2>Tabs</h2>
          <Tabs />
        </section>

        <section>
          <h2>TextArea</h2>
          <TextArea />
        </section>

      </div>
    );
  }
}

const rootElement = document.createElement('div');
document.body.insertBefore(rootElement, document.body.firstChild);

ReactDOM.render(<App />, rootElement);
